﻿namespace OrbitX.AAA.DataAccess
{
    public static class GameRepository
    {
        public const string SelectGame =
            "SELECT `id`, `title`, `description`, `developer_id`, `status` FROM games";
        public const string SelectGameWithSecret =
            "SELECT `id`, `title`, `description`, `developer_id`, `status`, `secret_key` FROM games";
        public const string ValidateGame =
            "SELECT COUNT(`id`) FROM games";

    }
}