﻿namespace OrbitX.AAA.DataAccess
{
    public static class GameserverRepository
    {
        public const string SelectGameserver =
            "SELECT `id`, `developer_id`, `title`, `description`, `status` FROM servers";
        public const string SelectGameserverWithSecret =
            "SELECT `id`, `developer_id`, `title`, `description`, `status`, `secret_key` FROM servers";
        public const string ValidateGameserver =
            "SELECT COUNT(`id`) FROM servers";
        public const string SelectAuthorizedGames =
            "SELECT `game_id`, `ccu_limit` FROM servers_games";
        public const string SelectGameserverWithAuthorizedGames =
            "SELECT s.*, g.* FROM servers s JOIN servers_games g ON s.id = g.server_id";
    }
}