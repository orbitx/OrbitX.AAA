using System;
using Microsoft.Extensions.Options;
using OrbitX.AAA.Models;
using RestSharp;
using RestSharp.Authenticators;
using Method = RestSharp.Method;

namespace OrbitX.AAA.Notifications
{
    public class EmailGateway
    {
        public string APIKey { get; set; }
        public string Domain { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }

        public EmailGateway(IOptions<ApiSettings> settings)
        {
            var apisettings = settings.Value;
            APIKey = apisettings.EmailApiKey;
            Domain = apisettings.EmailDomain;
            SenderName = apisettings.EmailSenderName;
            SenderEmail = apisettings.EmailSenderMailAddress;
        }
        
        public void SendEmail(string to, string subject, string body)
        {
            var client = new RestClient
            {
                BaseUrl = new Uri("https://api.mailgun.net/v3"),
                Authenticator = new HttpBasicAuthenticator(
                    "api", APIKey)
            };

            var request = new RestRequest();
            request.AddParameter("domain",
                                Domain, ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", $"{SenderName} <{SenderEmail}>");
            request.AddParameter("to", to);
            request.AddParameter("subject", subject);
            request.AddParameter("html", body);
            request.Method = Method.POST;
            client.ExecuteAsync(request, SentHandler);
        }

        private static void SentHandler(IRestResponse restResponse, RestRequestAsyncHandle restRequestAsyncHandle)
        {
            //throw new NotImplementedException();
        }
    }
}