﻿namespace OrbitX.AAA.Models
{
    public class Developer
    {
        public long Id { get; set; }
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string Email { get; set; } = "";
        public string Password { get; set; }
        public string Phone { get; set; } = "";
        public string Affiliation { get; set; } = "";
        public ActorStatus Status { get; set; }
    }
}