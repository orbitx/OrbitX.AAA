﻿using System.Collections.Generic;

namespace OrbitX.AAA.Models
{
    public class UserSkill
    {
        public string Title { get; set; }
        public double Score { get; set; }
        public double ScoreMean { get; set; }
        public double ScoreDeviation { get; set; }
    }

    public class User
    {
        public long Id { get; set; }
        public string Email { get; set; } = "";
        public string Phone { get; set; } = "";
        public string NickName { get; set; } = "";
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string Password { get; set; }
        public double Score { get; set; }
        public double ScoreMean { get; set; }
        public double ScoreDeviation { get; set; }
        public long CreatedBy { get; set; }
        public ActorStatus Status { get; set; }
        public List<UserSkill> Skills { get; set; } = new List<UserSkill>();
        public List<string> Achievements { get; set; } = new List<string>();
    }
}