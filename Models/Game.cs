﻿namespace OrbitX.AAA.Models
{
    public class Game
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long DeveloperId { get; set; }
        public string SecretKey { get; set; }
        public ActorStatus Status { get; set; }
    }
}