﻿namespace OrbitX.AAA.Models
{
    public enum Role
    {
        User,
        Developer,
        Game,
        GameServer
    }
}