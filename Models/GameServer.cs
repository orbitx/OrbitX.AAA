﻿using System.Collections.Generic;

namespace OrbitX.AAA.Models
{
    public class AuthorizedGame
    {
        public long GameId { get; set; }
        public long CcuLimit { get; set; }
    }
    
    public class GameServer
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long DeveloperId { get; set; }
        public string SecretKey { get; set; }
        public ActorStatus Status { get; set; }
        public List<AuthorizedGame> AuthorizedGames { get; set; }
    }
}