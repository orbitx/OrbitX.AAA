﻿namespace OrbitX.AAA.Models
{
    public enum ErrorCode
    {
        NotActivated = 41,
        InvalidCredentials = 42,
        IdentifierNotSpecified = 43
    }
}