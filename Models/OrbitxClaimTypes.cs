﻿namespace OrbitX.AAA.Models
{
    public static class OrbitxClaimTypes
    {
        public const string Role = "orbitx.role";
        public const string Id = "orbitx.id";
        public const string FirstName = "orbitx.fname";
        public const string LastName = "orbitx.lname";
        public const string Status = "orbitx.status";
        public const string Affiliation = "orbitx.affiliation";
        public const string Email = "orbitx.email";
        public const string Phone = "orbitx.phone";
        public const string NickName = "orbitx.nickname";
        public const string Loginer = "orbitx.loginer";
        public const string Title = "orbitx.title";
        public const string Description = "orbitx.description";
        public const string AuthorizedGames = "orbitx.authorized_games";
    }
}