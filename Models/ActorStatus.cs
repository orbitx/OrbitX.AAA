﻿namespace OrbitX.AAA.Models
{
    public enum ActorStatus
    {
        Active,
        Inactive,
        Closed,
        Removed
    }
}