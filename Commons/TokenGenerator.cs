﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace OrbitX.AAA.Commons
{
    public class TokenGenerator : Controller
    {
        protected JwtSecurityToken GetToken(Claim[] claims, DateTime expireAt, string securityKey)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            return new JwtSecurityToken(
                issuer: "aaa.orbitx.ir",
                audience: "aaa.orbitx.ir",
                claims: claims,
                expires: expireAt,
                signingCredentials: creds);
        }
    }
}