﻿using System;
using System.Security.Cryptography;

namespace OrbitX.AAA.Commons
{
    public static class RandomGenerator
    {
        private const string AllowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789!@#$%^&*()?_-";
        private static readonly RNGCryptoServiceProvider Generator = new RNGCryptoServiceProvider();

        private static int RandomBetween(int minimumValue, int maximumValue)
        {
            var randomNumber = new byte[1];
            Generator.GetBytes(randomNumber);
            var asciiValueOfRandomCharacter = Convert.ToDouble(randomNumber[0]);
            var multiplier = Math.Max(0, asciiValueOfRandomCharacter/255d - 0.00000000001d);
            var range = maximumValue - minimumValue + 1;
            var randomValueInRange = Math.Floor(multiplier * range);
            return (int)(minimumValue + randomValueInRange);
        }
        
        public static string GetString(int stringLength, int startIndex = 0, int? endIndex = null)
        {
            if (endIndex == null)
                endIndex = AllowedChars.Length;
            
            var chars = new char[stringLength];

            for (var i = 0; i < stringLength; i++)
            {
                chars[i] = AllowedChars[RandomBetween(startIndex, endIndex.Value - 1)];
            }

            return new string(chars);
        }
        
        public static string GetAlphanumericalString(int stringLength)
        {
            return GetString(stringLength, 0, 60);
        }
                
        public static string GetAlphabeticString(int stringLength)
        {
            return GetString(stringLength, 0, 50);
        }
        
        public static string GetNumericalString(int stringLength)
        {
            return GetString(stringLength, 50, 60);
        }
    }
}