USE `orbitx_aaa`;
ALTER TABLE `users` ADD COLUMN `created_by` BIGINT UNSIGNED;
ALTER TABLE `users` ADD CONSTRAINT  `fk__users__created_by` FOREIGN KEY (`created_by`) REFERENCES `games` (`id`);
CREATE INDEX idx_games_secret_key ON games (secret_key);
CREATE INDEX idx_servers_secret_key ON servers (secret_key);
ALTER TABLE servers ADD title VARCHAR(256) NULL;
ALTER TABLE servers ADD description VARCHAR(2048) NULL;
ALTER TABLE servers
  MODIFY COLUMN title VARCHAR(256) AFTER id,
  MODIFY COLUMN description VARCHAR(2048) AFTER title;
ALTER TABLE users
  MODIFY COLUMN created_by BIGINT(20) unsigned AFTER phone;
ALTER TABLE servers_games ADD status ENUM('active', 'inactive', 'closed', 'removed') DEFAULT 'inactive' NOT NULL;
ALTER TABLE servers_games ADD id BIGINT(20) NOT NULL PRIMARY KEY AUTO_INCREMENT;
ALTER TABLE servers_games
  MODIFY COLUMN id BIGINT(20) NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE servers_games AUTO_INCREMENT=1000;
ALTER TABLE servers_games ADD status_changed_at TIMESTAMP DEFAULT now() NULL;
ALTER TABLE servers_games ADD status_message VARCHAR(4096) NULL;
ALTER TABLE servers_games ADD created_at TIMESTAMP DEFAULT now() NULL;
ALTER TABLE servers_games ADD modified_at TIMESTAMP DEFAULT now() NULL ON UPDATE now();
CREATE INDEX idx__servers_games__status       ON servers_games (status);
CREATE INDEX idx__servers_games__created_at   ON servers_games (created_at);
ALTER TABLE servers_games ADD ccu_limit BIGINT DEFAULT 10 NULL;
ALTER TABLE servers_games
  MODIFY COLUMN ccu_limit BIGINT DEFAULT 10 AFTER game_id;
CREATE UNIQUE INDEX idx__servers__secret_key ON servers (secret_key);
ALTER TABLE servers ADD refresh_token VARCHAR(128) NULL;
CREATE UNIQUE INDEX idx__servers__refresh_token ON servers (refresh_token);
DROP INDEX idx_servers_secret_key ON servers;
ALTER TABLE servers
  MODIFY COLUMN refresh_token VARCHAR(128) AFTER latest_token_issued_at;