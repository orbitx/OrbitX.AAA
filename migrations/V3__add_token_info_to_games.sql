ALTER TABLE games ADD latest_token_issued_at TIMESTAMP NULL;
ALTER TABLE games
  MODIFY COLUMN latest_token_issued_at TIMESTAMP NULL AFTER developer_id;
ALTER TABLE games ADD refresh_token VARCHAR(128) NULL;
ALTER TABLE games
  MODIFY COLUMN refresh_token VARCHAR(128) NULL AFTER latest_token_issued_at;
CREATE UNIQUE INDEX idx__games__refresh_token ON games (refresh_token);
DROP INDEX idx_games_secret_key ON games;
CREATE INDEX idx__games__secret_key ON games (secret_key);