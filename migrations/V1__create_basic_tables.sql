DROP DATABASE IF EXISTS `orbitx_aaa`;
CREATE DATABASE `orbitx_aaa` CHARACTER SET = `utf8` COLLATE = `utf8_general_ci`;
USE `orbitx_aaa`;

CREATE TABLE `users`
(
    `id`                        SERIAL,
    `nickname`                  VARCHAR(64),
    `firstname`                 VARCHAR(64),
    `lastname`                  VARCHAR(64),
    `email`                     VARCHAR(128),
    `temp_email`                VARCHAR(128),
    `email_validation_token`	VARCHAR(128),
    `password`                  VARCHAR(128) NOT NULL,
    `phone`                     VARCHAR(24),
    `progress_mean`             DOUBLE DEFAULT 0,
    `progress_deviation`        DOUBLE DEFAULT 0,
    `progress`                  DOUBLE DEFAULT 0,
    `latest_token_issued_at`    TIMESTAMP DEFAULT 0,
    `refresh_token`             VARCHAR(128),
    `status`                    ENUM('active', 'inactive', 'closed', 'removed') NOT NULL DEFAULT 'active',
    `status_changed_at`         TIMESTAMP,
    `status_message`            VARCHAR(4096),
    `created_at`                TIMESTAMP DEFAULT now(),
    `modified_at`               TIMESTAMP DEFAULT now() ON UPDATE now(),
    PRIMARY KEY `pk__users__id`         (`id`),
    UNIQUE KEY  `uk__users__email`      (`email`),
    UNIQUE KEY  `uk__users__temp__email` (`temp_email`),
    UNIQUE KEY  `uk__users__phone`      (`phone`),
    UNIQUE KEY  `uk__users__refresh_token`      (`refresh_token`),
    UNIQUE KEY  `uk__users__email_validation_token` (`email_validation_token`)
) AUTO_INCREMENT=1000;

CREATE INDEX idx__users__status     ON users (status);
CREATE INDEX idx__users__created_at ON users (created_at);


CREATE TABLE developers
(
    `id`                        SERIAL,
    `firstname`                 VARCHAR(64),
    `lastname`                  VARCHAR(64),
    `email`                     VARCHAR(128),
    `temp_email`                VARCHAR(128),
    `email_validation_token`	VARCHAR(128),
    `password`                  VARCHAR(128)  NOT NULL,
    `phone`                     VARCHAR(24),
    `affiliation`               VARCHAR(50),
    `latest_token_issued_at`    TIMESTAMP DEFAULT 0,
    `refresh_token`             VARCHAR(128),
    `status`                    ENUM('active', 'inactive', 'closed', 'removed') NOT NULL DEFAULT 'inactive',
    `status_changed_at`         TIMESTAMP DEFAULT now(),
    `status_message`            VARCHAR(4096),
    `created_at`                TIMESTAMP DEFAULT now(),
    `modified_at`               TIMESTAMP DEFAULT now() ON UPDATE now(),
    PRIMARY KEY `pk__developers__id`    (`id`),
    UNIQUE KEY  `uk__developers__email` (`email`),
    UNIQUE KEY  `uk__users__temp__email` (`temp_email`),
    UNIQUE KEY  `uk__developers__phone` (`phone`),
    UNIQUE KEY  `uk__developers__refresh_token`      (`refresh_token`),
    UNIQUE KEY  `uk__users__email_validation_token` (`email_validation_token`)
) AUTO_INCREMENT=1000;

CREATE INDEX idx__developers__status        ON developers (status);
CREATE INDEX idx__developers__created_at    ON developers (created_at);


CREATE TABLE servers
(
    `id`                        SERIAL,
    `developer_id`              BIGINT UNSIGNED NOT NULL,
    `latest_token_issued_at`    TIMESTAMP DEFAULT 0,
    `status`                    ENUM('active', 'inactive', 'closed', 'removed') NOT NULL DEFAULT 'active',
    `status_changed_at`         TIMESTAMP DEFAULT now(),
    `status_message`            VARCHAR(4096),
    `created_at`                TIMESTAMP DEFAULT now(),
    `modified_at`               TIMESTAMP DEFAULT now() ON UPDATE now(),
    `secret_key`                VARCHAR(128),
    PRIMARY KEY `pk__servers__id`                       (`id`),
    CONSTRAINT  `fk__servers__developer_id` FOREIGN KEY (`developer_id`) REFERENCES `developers` (`id`)
) AUTO_INCREMENT=1000;

CREATE INDEX idx__servers__status       ON servers (status);
CREATE INDEX idx__servers__created_at   ON servers (created_at);

CREATE TABLE games
(
    `id`                        SERIAL,
    `title`                     VARCHAR(256),
    `description`               VARCHAR(2048),
    `developer_id`              BIGINT UNSIGNED NOT NULL,
    `status`                    ENUM('active', 'inactive', 'closed', 'removed') NOT NULL DEFAULT 'active',
    `status_changed_at`         TIMESTAMP DEFAULT now(),
    `status_message`            VARCHAR(4096),
    `created_at`                TIMESTAMP DEFAULT now(),
    `modified_at`               TIMESTAMP DEFAULT now() ON UPDATE now(),
    `secret_key`                VARCHAR(128),
    PRIMARY KEY `pk__games__id`                         (`id`),
    CONSTRAINT `fk__games__developer_id`    FOREIGN KEY (`developer_id`)    REFERENCES `developers` (`id`)
) AUTO_INCREMENT=1000;

CREATE INDEX idx__games__status     ON games (status);
CREATE INDEX idx__games__created_at ON games (created_at);


CREATE TABLE skills (
    `id`        SERIAL,
    `game_id`   BIGINT UNSIGNED NOT NULL,
    `title`     VARCHAR(128),
    PRIMARY KEY `pk__skills__id`                        (`id`),
    UNIQUE KEY  `uk__skills__progress_game`             (`game_id`, `title`),
    CONSTRAINT  `fk__skills__game_id`       FOREIGN KEY (`game_id`) REFERENCES `games` (`id`)
) AUTO_INCREMENT=1000;


CREATE TABLE achivements (
    `id`        SERIAL,
    `game_id`   BIGINT UNSIGNED NOT NULL,
    `title`     VARCHAR(128),
    PRIMARY KEY `pk__achivements__id`                        (`id`),
    UNIQUE KEY  `uk__achivements__progress_game`             (`game_id`, `title`),
    CONSTRAINT  `fk__achivements__game_id`       FOREIGN KEY (`game_id`) REFERENCES `games` (`id`)
) AUTO_INCREMENT=1000;


CREATE TABLE users_games (
    `user_id`    BIGINT UNSIGNED NOT NULL,
    `game_id`    BIGINT UNSIGNED NOT NULL,
    UNIQUE KEY `uk__users_games__user_game`                 (`user_id`, `game_id`),
    CONSTRAINT `fk__users_games__user_id`   FOREIGN KEY     (`user_id`) REFERENCES `users` (`id`),
    CONSTRAINT `fk__users_games__game_id`   FOREIGN KEY     (`game_id`) REFERENCES `games` (`id`)
);


CREATE TABLE servers_games (
    `server_id`     BIGINT UNSIGNED NOT NULL,
    `game_id`       BIGINT UNSIGNED NOT NULL,
    UNIQUE KEY `uk__servers_games__server_game`             (`server_id`, `game_id`),
    CONSTRAINT `fk__servers_games__server_id`   FOREIGN KEY (`server_id`)   REFERENCES `servers`    (`id`),
    CONSTRAINT `fk__servers_games__game_id`     FOREIGN KEY (`game_id`)     REFERENCES `games`      (`id`)
);


CREATE TABLE users_skills (
    `user_id`           BIGINT UNSIGNED NOT NULL,
    `skill_id`          BIGINT UNSIGNED NOT NULL,
    `score`             DOUBLE DEFAULT 0,
    `score_mean`        DOUBLE DEFAULT 0,
    `score_deviation`   DOUBLE DEFAULT 0,
    `modified_at`       TIMESTAMP DEFAULT now() ON UPDATE now(),
    UNIQUE KEY `uk__users_skills__user_progress`            (`user_id`, `skill_id`),
    CONSTRAINT `fk__users_skills__user_id`      FOREIGN KEY (`user_id`)     REFERENCES `users`      (`id`),
    CONSTRAINT `fk__users_skills__progress_id`  FOREIGN KEY (`skill_id`)    REFERENCES `skills`     (`id`)
);


CREATE TABLE users_achivements(
    `user_id`       BIGINT UNSIGNED NOT NULL,
    `achivement_id` BIGINT UNSIGNED NOT NULL,
    `achived_at`    TIMESTAMP DEFAULT now(),
    UNIQUE KEY `uk__users_achivements__user_achivement`             (`user_id`, `achivement_id`),
    CONSTRAINT `fk__users_achivements__user_id`         FOREIGN KEY (`user_id`)         REFERENCES `users`       (`id`),
    CONSTRAINT `fk__users_achivements__achivement_id`   FOREIGN KEY (`achivement_id`)   REFERENCES `achivements` (`id`)
);
