﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using OrbitX.AAA.Commons;
using OrbitX.AAA.DataAccess;
using OrbitX.AAA.Models;

namespace OrbitX.AAA.Controllers
{
    [Route("api/v1/token/game")]
    public class GameTokenController : TokenGenerator
    {
        private ApiSettings Settings { get; }

        public class GameTokenRequest
        {
            public string SecretKey { get; set; } = null;
        }

        public GameTokenController(IOptions<ApiSettings> settings)
        {
            Settings = settings.Value;
        }

        private async Task<IActionResult> GamesTokenBody(string whereClause, object param = null, bool refreshToken = true)
        {
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                var completeQuery =
                    $"{GameRepository.SelectGame} WHERE {whereClause}";
                var game = (await conn.QueryAsync<Game>(completeQuery, param)).FirstOrDefault();
                if (game != null)
                {
                    if (game.Status != ActorStatus.Active)
                    {
                        return new BadRequestObjectResult(new
                        {
                            message = "The specified game is not activated",
                            error_code = ErrorCode.NotActivated,
                            current_date = DateTime.Now
                        });
                    }

                    var rtoken = RandomGenerator.GetString(Settings.RefreshTokenLength);
                    var rowsAffected = await conn.ExecuteAsync(
                        "UPDATE games SET `latest_token_issued_at` = now() , `refresh_token`=password(@rtoken) WHERE `id`=@id",
                        new {rtoken, id = game.Id});

                    if (rowsAffected == 0)
                        return new NotFoundObjectResult("did not found such game");
                    
                    var claims = new[]
                    {
                        new Claim(ClaimTypes.Role, Role.Game.ToString()),
                        new Claim(OrbitxClaimTypes.Role, Role.Game.ToString()),
                        new Claim(OrbitxClaimTypes.Id, game.Id.ToString()),
                        new Claim(OrbitxClaimTypes.Title, game.Title),
                        new Claim(OrbitxClaimTypes.Description, game.Description),
                        new Claim(OrbitxClaimTypes.Status, game.Status.ToString()),
                    };

                    return new OkObjectResult(new
                    {
                        message = "OK",
                        access_token = new JwtSecurityTokenHandler().WriteToken(GetToken(claims,
                            DateTime.Now.AddMinutes(Settings.AccessTokenExpirationTime), Settings.SecurityKey)),
                        refresh_token = rtoken,
                        current_date = DateTime.Now,
                        access_expiration_time = DateTime.Now.AddMinutes(Settings.AccessTokenExpirationTime),
                        refresh_expiration_time = DateTime.Now.AddMinutes(Settings.RefreshTokenExpirationTime)
                    });
                }

                return new BadRequestObjectResult(new
                {
                    message = "Invalid credentials",
                    error_code = ErrorCode.InvalidCredentials,
                    current_date = DateTime.Now
                });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> GameRequestToken([FromBody] GameTokenRequest request)
        {
            var whereClause = "";
            object param;

            if (request.SecretKey != null)
            {
                whereClause += "`secret_key` = @secret";
                param = new { secret = request.SecretKey };
            }
            else
            {
                return new BadRequestObjectResult(new
                {
                    message = "secretkey should be specified",
                    error_code = ErrorCode.IdentifierNotSpecified,
                    current_date = DateTime.Now
                });
            }

            return await GamesTokenBody(whereClause, param);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("refresh/{refreshToken}")]
        public async Task<IActionResult> GameRefreshToken([FromRoute] string refreshToken)
        {
            var whereClause =
                $"`refresh_token`=password(@rtoken) AND DATE_ADD(`latest_token_issued_at`, INTERVAL {Settings.RefreshTokenExpirationTime} MINUTE) > now()";
            object param = new { rtoken = refreshToken };
            return await GamesTokenBody(whereClause, param);
        }

    }
}