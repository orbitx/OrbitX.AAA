﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using OrbitX.AAA.Commons;
using OrbitX.AAA.DataAccess;
using OrbitX.AAA.Models;

namespace OrbitX.AAA.Controllers
{
    [Route("api/v1/gameserver")]
    public class GameServerController : Controller
    {
        private ApiSettings Settings { get; }

        public GameServerController(IOptions<ApiSettings> settings)
        {
            Settings = settings.Value;
        }

        [HttpPost]
        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Post([FromBody] GameServer gameserver)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            gameserver.DeveloperId = developerId;
            var param = "";
            var values = "";
            var first = true;
            if (!string.IsNullOrEmpty(gameserver.Title))
            {
                param += "`title`";
                values += "@Title";
                first = false;
            }
            if (!string.IsNullOrEmpty(gameserver.Description))
            {
                param += (first ? "" : ",") + "`description`";
                values += (first ? "" : ",") + "@Description";
                if (first) first = false;
            }
            
            param += (first ? "" : ",") + "`developer_id`";
            values += (first ? "" : ",") + "@DeveloperId";
            if (first) first = false;

            gameserver.SecretKey = RandomGenerator.GetString(Settings.GameServerSecretLength);
            param += ",`secret_key`";
            values += ",@SecretKey";

            var query = $"INSERT INTO servers ({param}) VALUES ({values}); SELECT LAST_INSERT_ID();";
            long genId;
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    genId = (await conn.QueryAsync<long>(query, gameserver)).Single();
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new { message = e.Message });
                }
            }

            return new OkObjectResult(new { id = genId, secret = gameserver.SecretKey });
        }

        [HttpPatch]
        [Authorize(Roles = "Developer")]
        [Route("{id}")]
        public async Task<IActionResult> Patch([FromBody] GameServer gameserver, [FromRoute] long id)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            gameserver.DeveloperId = developerId;
            gameserver.Id = id;
            var query = "UPDATE servers SET ";
            var first = true;
            if (!string.IsNullOrEmpty(gameserver.Title))
            {
                query += "`title`=@Title";
                first = false;
            }
            if (!string.IsNullOrEmpty(gameserver.Description))
            {
                query += (first ? "" : ",") + "`description`=@Description";
                if (first) first = false;
            }
            query += " WHERE `developer_id`=@DeveloperId AND `id`=@Id";
            
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    if (await conn.ExecuteAsync(query, gameserver) > 0)
                        return new OkResult();
                    return new NotFoundObjectResult(new {message = "did not found such belonging gameserver"});
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new { message = e.Message });
                }
            }
        }

        [HttpPatch]
        [Authorize(Roles = "Developer")]
        [Route("{id}/resetsecret")]
        public async Task<IActionResult> ResetSecret([FromRoute] long id)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            var secretKey = RandomGenerator.GetString(Settings.GameServerSecretLength);
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                if (await conn.ExecuteAsync(
                        "UPDATE servers SET `secret_key`=@SecretKey WHERE `id`=@Id AND `developer_id`=@Developer",
                        new {SecretKey = secretKey, Developer = developerId, Id = id}) > 0)
                    return new OkObjectResult(new {new_secret_key = secretKey});
                return new NotFoundObjectResult(new {message = "did not found such belonging gameserver"});
            }
        }
        
        [HttpGet]
        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Get()
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            //TODO: include registered games
            var query = $"{GameserverRepository.SelectGameserver} WHERE `developer_id`=@developer";
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    return new OkObjectResult(await conn.QueryAsync<GameServer>(query, new {developer = developerId}));
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new {message = e.Message});
                }
            }
        }

        [HttpGet]
        [Authorize(Roles = "Developer")]
        [Route("{id}")]
        public async Task<IActionResult> Get([FromRoute] long id)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            var query = $"{GameserverRepository.SelectGameserverWithSecret} WHERE `developer_id`=@developer AND `id`=@gid";
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    return new OkObjectResult((await conn.QueryAsync<GameServer>(query,
                        new {developer = developerId, gid = id})).FirstOrDefault());
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new {message = e.Message});
                }
            }
        }

        [HttpPost]
        [Authorize(Roles = "Developer")]
        [Route("{serverid}/register/{gameid}")]
        public async Task<IActionResult> RegisterGame([FromRoute] long serverid, [FromRoute] long gameid)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    if ((await conn.QueryAsync<int>($"{GameRepository.ValidateGame} WHERE `developer_id`={developerId}"))
                        .First() == 0)
                        return new NotFoundObjectResult(new {message = "did not found such belonging game"});

                    if ((await conn.QueryAsync<int>(
                            $"{GameserverRepository.ValidateGameserver} WHERE `developer_id`={developerId}")).First() == 0)
                        return new NotFoundObjectResult(new {message = "did not found such belonging game"});

                    await conn.ExecuteAsync(
                        $"INSERT INTO servers_games (`server_id`, `game_id`) VALUES ({serverid}, {gameid})");
                    return new OkResult();
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new {message = e.Message});
                }
            }
        }
        
        [HttpGet]
        [Authorize(Roles = "GameServer")]
        [Route("authorized_games")]
        public async Task<IActionResult> GetAuthorizedGames()
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var gameId))
                return new NotFoundObjectResult(new {message = "did not found gameserver id in claims"});

            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    return new OkObjectResult(
                        await conn.QueryAsync<AuthorizedGame>(
                            $"{GameserverRepository.SelectAuthorizedGames} WHERE `server_id`={gameId} AND `status`='active'"));
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new {message = e.Message});
                }
            }
        }
    }
}