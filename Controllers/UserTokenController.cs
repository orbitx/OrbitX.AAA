﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using OrbitX.AAA.Commons;
using OrbitX.AAA.DataAccess;
using OrbitX.AAA.Models;

namespace OrbitX.AAA.Controllers
{
    [Route("api/v1/token/user")]
    public class UserTokenController : TokenGenerator
    {
        private ApiSettings Settings { get; }

        public UserTokenController(IOptions<ApiSettings> settings)
        {
            Settings = settings.Value;
        }

        public class UserTokenRequest
        {
            public long? Id { get; set; } = null;
            public string Email { get; set; } = null;
            public string Phone { get; set; } = null;
            public string Password { get; set; }
        }

        private async Task<IActionResult> UserTokenBody(string whereClause, object param = null)
        {
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                var completeQuery =
                    $"{UserRepository.SelectUser} WHERE {whereClause}";
                var user = (await conn.QueryAsync<User>(completeQuery, param)).FirstOrDefault();
                if (user != null)
                {
                    if (user.Status != ActorStatus.Active)
                    {
                        return new BadRequestObjectResult(new
                        {
                            message = "The specified user is not activated",
                            error_code = (int) ErrorCode.NotActivated,
                            current_date = DateTime.Now
                        });
                    }

                    var rtoken = RandomGenerator.GetString(Settings.RefreshTokenLength);
                    conn.Execute(
                        "UPDATE users SET `latest_token_issued_at` = now() , `refresh_token`=password(@rtoken) WHERE `id`=@id",
                        new {rtoken, id = user.Id});

                    var loginer = "";
                    if (User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Role)?.Value ==
                        Role.Game.ToString() &&
                        long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                            out var gameId))
                        loginer = gameId.ToString();

                    var claims = new[]
                    {
                        //TODO add skills and achievements in claims
                        new Claim(ClaimTypes.Role, Role.User.ToString()),
                        new Claim(OrbitxClaimTypes.Role, Role.User.ToString()),
                        new Claim(OrbitxClaimTypes.Id, user.Id.ToString()),
                        new Claim(OrbitxClaimTypes.FirstName, user.FirstName),
                        new Claim(OrbitxClaimTypes.LastName, user.LastName),
                        new Claim(OrbitxClaimTypes.NickName, user.NickName),
                        new Claim(OrbitxClaimTypes.Email, user.Email),
                        new Claim(OrbitxClaimTypes.Phone, user.Phone),
                        new Claim(OrbitxClaimTypes.Status, user.Status.ToString()),
                        new Claim(OrbitxClaimTypes.Loginer, loginer)
                    };

                    return new OkObjectResult(new
                    {
                        message = "OK",
                        access_token = new JwtSecurityTokenHandler().WriteToken(GetToken(claims,
                            DateTime.Now.AddMinutes(Settings.AccessTokenExpirationTime), Settings.SecurityKey)),
                        refresh_token = rtoken,
                        current_date = DateTime.Now,
                        access_expiration_time = DateTime.Now.AddMinutes(Settings.AccessTokenExpirationTime),
                        refresh_expiration_time = DateTime.Now.AddMinutes(Settings.RefreshTokenExpirationTime)
                    });
                }
                
                return new BadRequestObjectResult(
                    new
                    {
                        message = "Invalid credentials",
                        error_code = ErrorCode.InvalidCredentials,
                        current_date = DateTime.Now
                    });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> UserRequestToken([FromBody] UserTokenRequest request)
        {
            var whereClause = "`password` = password(@pass) AND ";
            object param;

            if (request.Id != null)
            {
                whereClause += "`id` = @id";
                param = new { id = request.Id, pass = request.Password };
            }
            else if (request.Email != null)
            {
                whereClause += "`email` = @email";
                param = new { email = request.Email, pass = request.Password };
            }
            else if (request.Phone != null)
            {
                whereClause += "`phone` = @phone";
                param = new { phone = request.Phone, pass = request.Password };
            }
            else
            {
                return new BadRequestObjectResult(new
                {
                    message = "Email or phone or ID should be specified",
                    error_code = ErrorCode.IdentifierNotSpecified,
                    current_date = DateTime.Now
                });
            }

            return await UserTokenBody(whereClause, param);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("refresh/{refreshToken}")]
        public async Task<IActionResult> UserRefreshToken([FromRoute] string refreshToken)
        {
            var whereClause =
                $"`refresh_token`=password(@rtoken) AND DATE_ADD(`latest_token_issued_at`, INTERVAL {Settings.RefreshTokenExpirationTime} MINUTE) > now()";
            object param = new { rtoken = refreshToken };
            return await UserTokenBody(whereClause, param);
        }
    }
}