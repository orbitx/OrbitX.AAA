using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Options;
using Dapper;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Language.Intermediate;
using OrbitX.AAA.Commons;
using OrbitX.AAA.DataAccess;
using OrbitX.AAA.Models;
using OrbitX.AAA.Notifications;

namespace OrbitX.AAA.Controllers
{
    [Route("api/v1/developer")]
    public class DeveloperController : Controller
    {
        private ApiSettings Settings { get; }
        private EmailGateway EmailGateway { get; set; }

        public DeveloperController(IOptions<ApiSettings> settings, EmailGateway emailGateway)
        {
            Settings = settings.Value;
            EmailGateway = emailGateway;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Developer developer)
        {
            var param = "";
            var values = "";
            var first = true;
            var etoken = "";
            if (!string.IsNullOrEmpty(developer.FirstName))
            {
                param += "`firstname`";
                values += "@FirstName";
                first = false;
            }
            if (!string.IsNullOrEmpty(developer.LastName))
            {
                param += (first ? "" : ",") + "`lastname`";
                values += (first ? "" : ",") + "@LastName";
                if (first) first = false;
            }
            if (!string.IsNullOrEmpty(developer.Email))
            {
                param += (first ? "" : ",") + "`temp_email`";
                values += (first ? "" : ",") + "@Email";
                if (first) first = false;

                etoken = RandomGenerator.GetAlphanumericalString(Settings.EmailTokenLength);
                param += ",`email_validation_token`";
                values += $",'{etoken}'";
            }
            if (!string.IsNullOrEmpty(developer.Phone))
            {
                param += (first ? "" : ",") + "`phone`";
                values += (first ? "" : ",") + "@Phone";
                if (first) first = false;
            }
            if (!string.IsNullOrEmpty(developer.Affiliation))
            {
                param += (first ? "" : ",") + "`affiliation`";
                values += (first ? "" : ",") + "@Affiliation";
                if (first) first = false;
            }

            param += (first ? "" : ",") + "`password`";
            values += (first ? "" : ",") + "password(@Password)";
            if (string.IsNullOrEmpty(developer.Password))
                developer.Password = RandomGenerator.GetString(Settings.AutogeneratedPasswordLength);

            var query = $"INSERT INTO developers ({param}) VALUES ({values});" +
                        "SELECT LAST_INSERT_ID();";
            long genId;
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    genId = (await conn.QueryAsync<long>(query, developer)).Single();

                    if (!string.IsNullOrEmpty(etoken))
                    {
                        EmailGateway.SendEmail(developer.Email, "Your Email Validation Link", 
                            $"Please click the following link to validate your account: " +
                            $"<a href='{Settings.ApiBaseAddress}/api/v1/developer/validate/{etoken}'>Validate Account</a>");
                        await conn.ExecuteAsync(
                            $"UPDATE developers SET email_validation_token = '{etoken}' WHERE id = {genId}");
                    }
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new { message = e.Message });
                }
            }

            return new OkObjectResult(new { id = genId, password = developer.Password });
        }

        [HttpGet]
        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Get()
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value, out var id))
                return new NotFoundResult();
            
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                var developer = (await conn
                    .QueryAsync<Developer>(
                        $"{DeveloperRepository.SelectDeveloper} WHERE id={id}"))
                    .FirstOrDefault();
                return new OkObjectResult(developer);
            }
        }

        [HttpPatch]
        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Patch([FromBody] Developer developer)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value, out var id))
                return new NotFoundResult();
            
            var query = "UPDATE developers SET ";
            var first = true;
            if (!string.IsNullOrEmpty(developer.FirstName))
            {
                query += "`firstname`=@FirstName";
                first = false;
            }
            if (!string.IsNullOrEmpty(developer.LastName))
            {
                query += (first ? "" : ",") + "`lastname`=@LastName";
                if (first) first = false;
            }
            if (!string.IsNullOrEmpty(developer.Email))
            {
                query += (first ? "" : ",") + "`email`=@Email";
                if (first) first = false;
            }
            if (!string.IsNullOrEmpty(developer.Phone))
            {
                query += (first ? "" : ",") + "`phone`=@Phone";
                if (first) first = false;
            }
            if (!string.IsNullOrEmpty(developer.Password))
            {
                query += (first ? "" : ",") + "`password`=password(@Password)";
                if (first) first = false;
            }

            if (first)
                return new BadRequestObjectResult(new { message = "At least one field should be patched" });

            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                query += " WHERE id=" + id;
                try
                {
                    if (await conn.ExecuteAsync(query, developer) > 0)
                        return new OkResult();

                    return new NotFoundObjectResult(new { message = "did not found such developer to patch" });
                }
                catch (MySqlException e)
                {
                    return new BadRequestObjectResult(new { message = e.Message });
                }
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("validate/{emailToken}")]
        public async Task<IActionResult> ValidateEmail([FromRoute]string emailToken)
        {
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                var rowinfect = await conn.ExecuteAsync(
                    "UPDATE developers SET status = 'active', `email` = `temp_email`, `email_validation_token` = NULL WHERE `email_validation_token` = @etoken", 
                    new { etoken = emailToken});
                if (rowinfect > 0)
                {
                    return Redirect($"{Settings.WebsiteBaseAddress}/validation.html");
                }
                return new BadRequestObjectResult(new { message = "Invalid token" });
            }
        }
        
        [HttpPost]
        [AllowAnonymous]
        [Route("forgot/{email}")]
        public async Task<IActionResult> ForgotPassword([FromRoute]string email)
        {
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                var etoken = RandomGenerator.GetAlphanumericalString(Settings.EmailTokenLength);
                
                var rowinfect = await conn.ExecuteAsync(
                    $"UPDATE developers SET `email_validation_token` = '{etoken}' WHERE `email` = @mail", 
                    new { mail = email});

                if (rowinfect <= 0) return new BadRequestObjectResult(new {message = "Invalid email"});
                EmailGateway.SendEmail(email, "Your Reset Password Link", 
                    "Please click the following link to reset your password: " +
                    $"<a href='{Settings.WebsiteBaseAddress}/resetpass.html#{etoken}'>Reset Password</a>");
                return new OkResult();
            }
        }

        public class ResetRequest
        {
            public string Token { get; set; }
            public string Password { get; set; }
        }
        
        [HttpPost]
        [AllowAnonymous]
        [Route("reset")]
        public async Task<IActionResult> ResetPassword([FromBody]ResetRequest request)
        {
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                var rowinfect = await conn.ExecuteAsync(
                    "UPDATE developers SET `password` = password(@pass), `email_validation_token` = NULL WHERE email_validation_token = @etoken", 
                    new { etoken = request.Token, pass = request.Password});
                
                if (rowinfect > 0)
                {
                    return new OkResult();
                }
                return new BadRequestObjectResult(new { message = "Invalid token" });
            }
        }
        
    }
}
