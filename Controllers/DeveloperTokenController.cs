using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using OrbitX.AAA.Commons;
using OrbitX.AAA.DataAccess;
using OrbitX.AAA.Models;

namespace OrbitX.AAA.Controllers
{
    [Route("api/v1/token/developer")]
    public class DeveloperTokenController : TokenGenerator
    {
        private ApiSettings Settings { get; }

        public class DeveloperTokenRequest
        {
            public long? Id { get; set; } = null;
            public string Email { get; set; } = null;
            public string Phone { get; set; } = null;
            public string Password { get; set; }
        }

        public DeveloperTokenController(IOptions<ApiSettings> settings)
        {
            Settings = settings.Value;
        }

        private async Task<IActionResult> DeveloperTokenBody(string whereClause, object param = null, bool refreshToken = true)
        {
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                var completeQuery =
                    $"{DeveloperRepository.SelectDeveloper} WHERE {whereClause}";
                var developer = (await conn.QueryAsync<Developer>(completeQuery, param)).FirstOrDefault();
                if (developer != null)
                {
                    if (developer.Status != ActorStatus.Active)
                    {
                        return new BadRequestObjectResult(new
                        {
                            message = "The specified developer is not activated",
                            error_code = ErrorCode.NotActivated,
                            current_date = DateTime.Now
                        });
                    }

                    var rtoken = RandomGenerator.GetString(Settings.RefreshTokenLength);
                    var rowsAffected = await conn.ExecuteAsync(
                        "UPDATE developers SET `latest_token_issued_at` = now() , `refresh_token`=password(@rtoken) WHERE `id`=@id",
                        new {rtoken, id = developer.Id});

                    if (rowsAffected == 0)
                        return new NotFoundObjectResult("did not found such developer");
                    
                    var claims = new[]
                    {
                        new Claim(ClaimTypes.Role, Role.Developer.ToString()),
                        new Claim(OrbitxClaimTypes.Role, Role.Developer.ToString()),
                        new Claim(OrbitxClaimTypes.Id, developer.Id.ToString()),
                        new Claim(OrbitxClaimTypes.FirstName, developer.FirstName),
                        new Claim(OrbitxClaimTypes.LastName, developer.LastName),
                        new Claim(OrbitxClaimTypes.Status, developer.Status.ToString()),
                        new Claim(OrbitxClaimTypes.Affiliation, developer.Affiliation),
                        new Claim(OrbitxClaimTypes.Email, developer.Email),
                        new Claim(OrbitxClaimTypes.Phone, developer.Phone)
                    };

                    return new OkObjectResult(new
                    {
                        message = "OK",
                        access_token = new JwtSecurityTokenHandler().WriteToken(GetToken(claims,
                            DateTime.Now.AddMinutes(Settings.AccessTokenExpirationTime), Settings.SecurityKey)),
                        refresh_token = rtoken,
                        current_date = DateTime.Now,
                        access_expiration_time = DateTime.Now.AddMinutes(Settings.AccessTokenExpirationTime),
                        refresh_expiration_time = DateTime.Now.AddMinutes(Settings.RefreshTokenExpirationTime)
                    });
                }

                return new BadRequestObjectResult(new
                {
                    message = "Invalid credentials",
                    error_code = ErrorCode.InvalidCredentials,
                    current_date = DateTime.Now
                });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> DeveloperRequestToken([FromBody] DeveloperTokenRequest request)
        {
            string whereClause =
                "`password` = password(@pass) AND ";
            object param;

            if (request.Id != null)
            {
                whereClause += "`id` = @id";
                param = new { id = request.Id, pass = request.Password };
            }
            else if (request.Email != null)
            {
                whereClause += "@email IN (`email`, `temp_email`)";
                param = new { email = request.Email, pass = request.Password };
            }
            else if (request.Phone != null)
            {
                whereClause += "`phone` = @phone";
                param = new { phone = request.Phone, pass = request.Password };
            }
            else
            {
                return new BadRequestObjectResult(new
                {
                    message = "Email or phone or ID should be specified",
                    error_code = ErrorCode.IdentifierNotSpecified,
                    current_date = DateTime.Now
                });
            }

            return await DeveloperTokenBody(whereClause, param);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("refresh/{refreshToken}")]
        public async Task<IActionResult> DeveloperRefreshToken([FromRoute] string refreshToken)
        {
            var whereClause =
                $"`refresh_token`=password(@rtoken) AND DATE_ADD(`latest_token_issued_at`, INTERVAL {Settings.RefreshTokenExpirationTime} MINUTE) > now()";
            object param = new { rtoken = refreshToken };
            return await DeveloperTokenBody(whereClause, param);
        }
    }
}
