﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using OrbitX.AAA.Commons;
using OrbitX.AAA.DataAccess;
using OrbitX.AAA.Models;

namespace OrbitX.AAA.Controllers
{
    [Route("api/v1/token/gameserver")]
    public class GameserverTokenController : TokenGenerator
    {
        private ApiSettings Settings { get; }

        public class GameserverTokenRequest
        {
            public string SecretKey { get; set; } = null;
        }

        public GameserverTokenController(IOptions<ApiSettings> settings)
        {
            Settings = settings.Value;
        }

        private async Task<IActionResult> GameserverTokenBody(string whereClause, object param = null, bool refreshToken = true)
        {
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                var lookup = new Dictionary<long, GameServer>();
                var completeQuery =
                    $"{GameserverRepository.SelectGameserverWithAuthorizedGames} WHERE {whereClause}";
                var gameserver = (await conn.QueryAsync<GameServer, AuthorizedGame, GameServer>(
                    completeQuery,
                    (s, g) =>
                    {
                        if (!lookup.TryGetValue(s.Id, out var gserver)) {
                            lookup.Add(s.Id, gserver = s);
                        }
                        if (gserver.AuthorizedGames == null) 
                            gserver.AuthorizedGames = new List<AuthorizedGame>();
                        gserver.AuthorizedGames.Add(g);
                        return gserver;
                    }, param)).FirstOrDefault();
                if (gameserver != null)
                {
                    if (gameserver.Status != ActorStatus.Active)
                    {
                        return new BadRequestObjectResult(new
                        {
                            message = "The specified gameserver is not activated",
                            error_code = ErrorCode.NotActivated,
                            current_date = DateTime.Now
                        });
                    }

                    var rtoken = RandomGenerator.GetString(Settings.RefreshTokenLength);
                    var rowsAffected = await conn.ExecuteAsync(
                        "UPDATE servers SET `latest_token_issued_at` = now() , `refresh_token`=password(@rtoken) WHERE `id`=@id",
                        new {rtoken, id = gameserver.Id});

                    if (rowsAffected == 0)
                        return new NotFoundObjectResult("did not found such gameserver");
                    
                    var claims = new[]
                    {
                        new Claim(ClaimTypes.Role, Role.GameServer.ToString()),
                        new Claim(OrbitxClaimTypes.Role, Role.GameServer.ToString()),
                        new Claim(OrbitxClaimTypes.Id, gameserver.Id.ToString()),
                        new Claim(OrbitxClaimTypes.Title, gameserver.Title),
                        new Claim(OrbitxClaimTypes.Description, gameserver.Description),
                        new Claim(OrbitxClaimTypes.Status, gameserver.Status.ToString()),
                        new Claim(OrbitxClaimTypes.AuthorizedGames, JsonConvert.SerializeObject(gameserver.AuthorizedGames))
                    };

                    return new OkObjectResult(new
                    {
                        message = "OK",
                        access_token = new JwtSecurityTokenHandler().WriteToken(GetToken(claims,
                            DateTime.Now.AddMinutes(Settings.AccessTokenExpirationTime), Settings.SecurityKey)),
                        refresh_token = rtoken,
                        current_date = DateTime.Now,
                        access_expiration_time = DateTime.Now.AddMinutes(Settings.AccessTokenExpirationTime),
                        refresh_expiration_time = DateTime.Now.AddMinutes(Settings.RefreshTokenExpirationTime)
                    });
                }

                return new BadRequestObjectResult(new
                {
                    message = "Invalid credentials",
                    error_code = ErrorCode.InvalidCredentials,
                    current_date = DateTime.Now
                });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> GameserverRequestToken([FromBody] GameserverTokenRequest request)
        {
            string whereClause = "";
            object param;

            if (request.SecretKey != null)
            {
                whereClause += "`secret_key` = @secret";
                param = new { secret = request.SecretKey };
            }
            else
            {
                return new BadRequestObjectResult(new
                {
                    message = "secretkey should be specified",
                    error_code = ErrorCode.IdentifierNotSpecified,
                    current_date = DateTime.Now
                });
            }

            return await GameserverTokenBody(whereClause, param);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("refresh/{refreshToken}")]
        public async Task<IActionResult> GameserverRefreshToken([FromRoute] string refreshToken)
        {
            var whereClause =
                $"`refresh_token`=password(@rtoken) AND DATE_ADD(`latest_token_issued_at`, INTERVAL {Settings.RefreshTokenExpirationTime} MINUTE) > now()";
            object param = new { rtoken = refreshToken };
            return await GameserverTokenBody(whereClause, param);
        }

    }
}