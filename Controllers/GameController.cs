﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using OrbitX.AAA.Commons;
using OrbitX.AAA.DataAccess;
using OrbitX.AAA.Models;

namespace OrbitX.AAA.Controllers
{
    [Route("api/v1/game")]
    public class GameController : Controller
    {
        private ApiSettings Settings { get; }

        public GameController(IOptions<ApiSettings> settings)
        {
            Settings = settings.Value;
        }

        [HttpPost]
        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Post([FromBody] Game game)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            game.DeveloperId = developerId;
            var param = "";
            var values = "";
            var first = true;
            if (!string.IsNullOrEmpty(game.Title))
            {
                param += "`title`";
                values += "@Title";
                first = false;
            }
            if (!string.IsNullOrEmpty(game.Description))
            {
                param += (first ? "" : ",") + "`description`";
                values += (first ? "" : ",") + "@Description";
                if (first) first = false;
            }
            
            param += (first ? "" : ",") + "`developer_id`";
            values += (first ? "" : ",") + "@DeveloperId";
            if (first) first = false;

            game.SecretKey = RandomGenerator.GetString(Settings.GameSecretLength);
            param += ",`secret_key`";
            values += ",@SecretKey";

            var query = $"INSERT INTO games ({param}) VALUES ({values}); SELECT LAST_INSERT_ID();";
            long genId;
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    genId = (await conn.QueryAsync<long>(query, game)).Single();
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new { message = e.Message });
                }
            }

            return new OkObjectResult(new { id = genId, secret = game.SecretKey });
        }

        [HttpPatch]
        [Authorize(Roles = "Developer")]
        [Route("{id}")]
        public async Task<IActionResult> Patch([FromBody] Game game, [FromRoute] long id)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            game.DeveloperId = developerId;
            game.Id = id;
            var query = "UPDATE games SET ";
            var first = true;
            if (!string.IsNullOrEmpty(game.Title))
            {
                query += "`title`=@Title";
                first = false;
            }
            if (!string.IsNullOrEmpty(game.Description))
            {
                query += (first ? "" : ",") + "`description`=@Description";
                if (first) first = false;
            }
            query += " WHERE `developer_id`=@DeveloperId AND `id`=@Id";
            
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    if (await conn.ExecuteAsync(query, game) > 0)
                        return new OkResult();
                    return new NotFoundObjectResult(new {message = "did not found such belonging game"});
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new { message = e.Message });
                }
            }
        }

        [HttpPatch]
        [Authorize(Roles = "Developer")]
        [Route("{id}/resetsecret")]
        public async Task<IActionResult> ResetSecret([FromRoute] long id)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            var secretKey = RandomGenerator.GetString(Settings.GameSecretLength);
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                if (await conn.ExecuteAsync(
                        "UPDATE games SET `secret_key`=@SecretKey WHERE `id`=@Id AND `developer_id`=@Developer",
                        new {SecretKey = secretKey, Developer = developerId, Id = id}) > 0)
                    return new OkObjectResult(new {new_secret_key = secretKey});
                return new NotFoundObjectResult(new {message = "did not found such belonging game"});
            }
        }
        
        [HttpGet]
        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Get()
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            var query = $"{GameRepository.SelectGame} WHERE `developer_id`=@developer";
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    return new OkObjectResult(await conn.QueryAsync<Game>(query, new {developer = developerId}));
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new {message = e.Message});
                }
            }
        }

        [HttpGet]
        [Authorize(Roles = "Developer")]
        [Route("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            var query = $"{GameRepository.SelectGameWithSecret} WHERE `developer_id`=@developer AND `id`=@gid";
            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    return new OkObjectResult((await conn.QueryAsync<Game>(query,
                        new {developer = developerId, gid = id})).FirstOrDefault());
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new {message = e.Message});
                }
            }
        }
        
        [HttpPost]
        [Authorize(Roles = "Developer")]
        [Route("{gameid}/register/{serverid}")]
        public async Task<IActionResult> RegisterGame([FromRoute] long serverid, [FromRoute] long gameid)
        {
            if (!long.TryParse(User.Claims.FirstOrDefault(x => x.Type == OrbitxClaimTypes.Id)?.Value,
                out var developerId))
                return new NotFoundObjectResult(new {message = "did not found developer id in claims"});

            using (var conn = new MySqlConnection(Settings.DbConnectionString))
            {
                try
                {
                    if ((await conn.QueryAsync<int>($"{GameRepository.ValidateGame} WHERE `developer_id`={developerId}"))
                        .First() == 0)
                        return new NotFoundObjectResult(new {message = "did not found such belonging game"});

                    if ((await conn.QueryAsync<int>(
                            $"{GameserverRepository.ValidateGameserver} WHERE `developer_id`={developerId}")).First() == 0)
                        return new NotFoundObjectResult(new {message = "did not found such belonging game"});

                    await conn.ExecuteAsync(
                        $"INSERT INTO servers_games (`server_id`, `game_id`) VALUES ({serverid}, {gameid})");
                    return new OkResult();
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(new {message = e.Message});
                }
            }
        }
    }
}